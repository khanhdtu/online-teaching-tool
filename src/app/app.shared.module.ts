import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AuthService } from './services/auth.service';
import { ToastrModule } from 'ngx-toastr';
import { FlexLayoutModule } from '@angular/flex-layout';
import { AmplifyModules, AmplifyAngularModule, AmplifyService } from 'aws-amplify-angular';
import Auth from '@aws-amplify/auth';

@NgModule({
    imports: [
        CommonModule,
        AmplifyAngularModule,
        ToastrModule.forRoot({
            timeOut: 3000,
            positionClass: 'toast-top-center',
            preventDuplicates: true,
        })
    ],
    declarations: [],
    providers: [
        AuthService,
        {
            provide: AmplifyService,
            useFactory: () => {
                return AmplifyModules({
                    Auth
                });
            }
        }
    ],
    exports: [
        CommonModule,
        FlexLayoutModule,
        FormsModule
    ]
})

export class SharedModule {
}