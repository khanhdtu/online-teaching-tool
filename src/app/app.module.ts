import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';
import { AppRoutingModule } from './app.routing.module';
import { NgModule } from '@angular/core';
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireAuthModule } from '@angular/fire/auth';
// import { AngularFireStorageModule } from '@angular/fire/storage';

import { AppComponent } from './app.component';
import { ConfigModule, ConfigLoader } from '@ngx-config/core'

import { appConfigFactory, firebaseConfigFactory } from './app.config.module';
import { SecretComponent } from './pages/secret/secret.component';
import { SharedModule } from './app.shared.module';

@NgModule({
  declarations: [
    AppComponent,
    SecretComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    RouterModule,
    AngularFireModule.initializeApp(firebaseConfigFactory),
    AngularFirestoreModule,
    AngularFireAuthModule,
    SharedModule,
    ConfigModule.forRoot({
      provide: ConfigLoader,
      useFactory: appConfigFactory
    })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
