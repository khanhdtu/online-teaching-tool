import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { LocalStorageHelper } from '../helpers/local-storage.helper';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  public storageHelper = new LocalStorageHelper;
  constructor(private router: Router) {}
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    if (this.storageHelper.getItem('token'))
    return true;
    return this.router.navigate(['/login'])
  }
}
