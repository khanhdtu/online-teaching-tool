export interface User {
    token?: string;
    uid?: string;
    email?: string;
    password?: string;
    avatar?: string;
    age?: string;
    gender?: boolean;
    account_type?: string;
    displayName?: string
}