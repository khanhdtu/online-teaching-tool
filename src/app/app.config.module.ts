import { ConfigLoader, ConfigStaticLoader } from '@ngx-config/core'

export function appConfigFactory(): ConfigLoader {
    return new ConfigStaticLoader({
        "system": {
            "name": "Smart Calc Application",
            "url": "http://localhost:8000"
        },
        "seo": {
            "pageTitle": "SEO Friendly"
        },
        "i18n": {
            "locale": "en"
        }
    });
}

export const firebaseConfigFactory = {
    apiKey: "AIzaSyAxxK-qLPbOvVgT9xN8nXFfpfz-FCpgsQk",
    authDomain: "smart-calc-cbc02.firebaseapp.com",
    databaseURL: "https://smart-calc-cbc02.firebaseio.com",
    projectId: "smart-calc-cbc02",
    storageBucket: "gs://smart-calc-cbc02.appspot.com",
    messagingSenderId: "140853548925"
}

export const awsConfigFactory = {
    "aws_project_region": "ap-southeast-1",
    "aws_cognito_identity_pool_id": "ap-southeast-1:72d92818-a420-4b8f-b108-ed2886f7f238",
    "aws_cognito_region": "ap-southeast-1",
    "aws_user_pools_id": "ap-southeast-1_0iOvvlQfd",
    "aws_user_pools_web_client_id": "62cibs8naptst6lnh7hc6qgi03",
    "oauth": {},
    "aws_content_delivery_bucket": "inalesting-20190621111415-hostingbucket-awsenv",
    "aws_content_delivery_bucket_region": "ap-southeast-1",
    "aws_content_delivery_url": "https://d1nxcye9p5seng.cloudfront.net"
}