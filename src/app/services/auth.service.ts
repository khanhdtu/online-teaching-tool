import { Injectable } from '@angular/core';
import { User } from '../models/user.model';
import { auth } from 'firebase/app';
import {
  AngularFirestore,
  AngularFirestoreDocument,
  AngularFirestoreCollection,
} from '@angular/fire/firestore';
import { AngularFireAuth } from '@angular/fire/auth';
import * as _ from 'lodash';
import { ToastrService } from 'ngx-toastr';
import { MessageConst } from 'src/app/constants/message.constant';
import { LocalStorageHelper } from 'src/app/helpers/local-storage.helper';
import { Router } from '@angular/router';
@Injectable({
  providedIn: 'root'
})
export class AuthService {
  public users: Array<User>
  public user: User;
  public storageHelper = new LocalStorageHelper
  constructor(
    private afs: AngularFirestore,
    private afAuth: AngularFireAuth,
    private router: Router,
    private toastService: ToastrService) {
      this.afs.collection<User>('users').valueChanges()
      .subscribe(users => this.users = users)
  }

  youAre(user: User) {
    const you = _.find(this.users, { ...user });
    this.storageHelper.setItem('profile', you);
    return you;
  }

  socialYouAre(user: User) {
    const you = this.users.find(e => {
      return e.uid === user.uid
    });
    this.storageHelper.setItem('profile', you)
    return you
  }

  login(user: User) {
    if (this.youAre(user)) {
      // A sample token
      this.storageHelper.setItem('token', 'Valid Acces_Token')
      return window.location.href = '/home'
    }
    this.toastService.error(
    `${MessageConst.label.auth + ' ' + MessageConst.status.Failed}`,
    `${MessageConst.more.LoginFailed}`)
  }

  socialLogin(provider: string) {
    return new Promise((resolve,reject) => {
      this.afAuth.auth.signInWithPopup(new auth[provider]())
      .then(res => {
        console.log(res)
        if (res.credential && res.credential['accessToken']) {
          let accType = '', photo = ''
          if (provider.includes('Facebook')) accType = 'Facebook Provider'
          if (provider.includes('Google')) accType = 'Google Provider'
          if (res.additionalUserInfo.profile['picture']['data']) photo = res.additionalUserInfo.profile['picture']['data']['url']
          else photo = res.additionalUserInfo.profile['picture'];
          const user: User = {
            token: res.credential['accessToken'],
            displayName: res.user.displayName,
            email: res.user.email,
            uid: res.user.uid,
            avatar: photo,
            account_type: accType
          }
          resolve(user);
        }
      })
    })
  }

  register(user: User) {
    this.afs.collection<User>('users')
    .add(user)
    .then(() => this.toastService.success('Register Successfully !'))
    .catch(() => this.toastService.error('Register Error Occured !'))
  }

  socialRegister(provider: string) {
    this.socialLogin(provider)
    .then(user => {
      this.afs.collection<User>('users')
      .add(user)
      .then(() => this.toastService.success('Register Successfully !'))
      .catch(() => this.toastService.error('Register Error Occured !'))
    })
  }

  handleSocialLogin(provider: string) {
    this.socialLogin(provider)
    .then(user => {
      if (this.socialYouAre(user)) return window.location.href = '/home';
      return this.toastService.error("You didn't register yet !")
    })
  }
}
