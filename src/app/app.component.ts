import { Component, ViewChild } from '@angular/core';
import { ToastContainerDirective } from 'ngx-toastr';
@Component({
  selector: 'app-root',
  template: `
    <div id="app" toastContainer>
    <router-outlet></router-outlet>
    </div>
  `
})
export class AppComponent {
  @ViewChild(ToastContainerDirective, {
    static: false
  }) toastContainer: ToastContainerDirective;
}
