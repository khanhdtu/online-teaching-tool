export const MessageConst = {
    status: {
        Success: 'Success !',
        Failed: 'Failed !',
        Warning: 'Warning !'
    },
    label: {
        auth: 'Authentication'
    },
    more: {
        LoginFailed: 'Email or password is incorrect'
    }
}