import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { SharedModule } from 'src/app/app.shared.module';
import { MatCardModule, MatIconModule } from '@angular/material';
import { AuthGuard } from 'src/app/guards/auth.guard';

@NgModule({
  declarations: [DashboardComponent],
  imports: [
    CommonModule,
    SharedModule,
    MatCardModule,
    MatIconModule,
    HomeRoutingModule
  ],
  providers: [
    AuthGuard
  ]
})
export class HomeModule { }
