import { Component, OnInit } from '@angular/core';
import { LocalStorageHelper } from '../../../../helpers/local-storage.helper';
import { AuthService } from 'src/app/services/auth.service';
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  public profile: any;
  public storageHelper = new LocalStorageHelper;
  constructor() {
    this.profile = this.storageHelper.getItem('profile')
  }

  ngOnInit() {
  }

}
