import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/models/user.model';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  public user: User;
  constructor(private authService: AuthService, private router: Router) {
    this.user = {
      email: '',
      password: '',
      displayName: 'Khanh',
      account_type: 'normal'
    }
  }

  onRegister() {
    this.authService.register(this.user)
  }

  onSocialRegister(provider) {
    this.authService.socialRegister(provider)
  }

  toLogin() {
    this.router.navigate(['/login'])
  }

  ngOnInit() {
  }
}
