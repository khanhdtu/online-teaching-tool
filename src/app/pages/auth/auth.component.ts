import { Component, OnInit, ViewChild } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { User } from 'src/app/models/user.model';
import { AwsAuthService } from 'src/app/services/aws-auth.service';
import { Router } from '@angular/router';
import { AmplifyService } from 'aws-amplify-angular';
@Component({
  selector: 'app-auth',
  template: `
    <amplify-authenticator [signUpConfig]="signUpConfig"></amplify-authenticator>
  `
})
export class AuthComponent implements OnInit {
  signedIn: boolean;
  user: any;
  greeting: string;
  signUpConfig = {
    header: 'My Customized Sign Up',
    hideAllDefaults: true,
    defaultCountryCode: '1',
    signUpFields: [
      {
        label: 'Username',
        key: 'username',
        required: true,
        displayOrder: 1,
        type: 'string'
      },
      {
        label: 'Email',
        key: 'email',
        required: true,
        displayOrder: 2,
        type: 'string',
      },
      {
        label: 'Password',
        key: 'password',
        required: true,
        displayOrder: 3,
        type: 'password'
      },
      {
        label: 'Phone Number',
        key: 'phone_number',
        required: true,
        displayOrder: 3,
        type: 'string'
      }
    ]
  }
  constructor(
    private authService: AuthService,
    private router: Router,
    private amplifyService: AmplifyService
  ) {
    this.user = {
      email: '',
      password: '',
      account_type: 'normal'
    }
    this.amplifyService.authStateChange$
      .subscribe(authState => {
        console.log('Changed:', authState)
        this.signedIn = authState.state === 'signedIn';
        if (!authState.user) {
          this.user = null;
        } else {
          this.user = authState.user;
          this.greeting = "Hello " + this.user.username;
        }
      });
  }

  onLogin() {
    this.authService.login(this.user)
  }

  onSocialLogin(provider) {
    this.authService.handleSocialLogin(provider)
  }

  toRegister() {
    this.router.navigate(['/register'])
  }

  ngOnInit() {
    console.log('Initial Auth Module')
  }  
}
