import { RouterModule, Routes } from '@angular/router';
import { SecretComponent } from './pages/secret/secret.component';
import { RegisterComponent } from './pages/auth/pages/register/register.component';

const routes: Routes = [
  {
    path:'',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  {
    path: '',
    children: [
      {
        path: '',
        loadChildren: () => import('./pages/auth/auth.module')
        .then(mod => mod.AuthModule)
      },
      // {
      //   path: 'register',
      //   component: RegisterComponent
      // },
      {
        path: 'home',
        loadChildren: () => import('./pages/home/home.module')
        .then(mod => mod.HomeModule)
      }
    ]
  },
  {
    path: '404',
    component: SecretComponent
  },
  {
    path:'**',
    redirectTo: '404',
  },
]

export const AppRoutingModule = RouterModule.forRoot(routes);
